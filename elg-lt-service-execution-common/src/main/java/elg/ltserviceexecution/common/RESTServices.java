package elg.ltserviceexecution.common;

/**
 * @author galanisd
 *
 */
public class RESTServices {

	public static final String ltService = "ltService";
	public static final String PROCESS_DOCUMENT_SERV = "/processDocument/{" + ltService + "}";
	public static final String PROCESS_TEXT = "/processText/{" + ltService + "}";
	public static final String PROCESS_AUDIO = "/processAudio/{" + ltService + "}";

	public static final String ASYNC_PREFIX = "/async";
	public static final String taskId = "taskId";
	public static final String POLL = "/poll/{" + taskId + "}";

	public static final String PROCESS_RESOURCE_SERV = "/processResource";
	
	
	public static final String REGISTER_SERVICE_SERV = "/registerService/{" + ltService + "}";
	public static final String UNREGISTER_SERVICE_SERV = "/unregisterService/{" + ltService + "}";
}
