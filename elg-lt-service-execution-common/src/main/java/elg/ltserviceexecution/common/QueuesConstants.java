package elg.ltserviceexecution.common;

/**
 * 
 * Queues Constants
 * 
 * @author galanisd
 *
 */
public class QueuesConstants {

	public static final String ELG_EXCHANGE = "ELG";
	public static final String ELG_EXCHANGE_ALTER = "ELGALTER";

	public static final String DEFAULT_REPLY_QUEUE = "replyqueue";
}
