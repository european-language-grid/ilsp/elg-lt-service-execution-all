package elg.ltserviceexecution.restserver;

import elg.ltserviceexecution.api.LTServiceExecution;
import elg.ltserviceexecution.common.RESTServices;
import eu.elg.model.*;
import eu.elg.model.requests.AudioRequest;
import eu.elg.model.requests.TextRequest;
import eu.elg.model.responses.AudioResponse;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.SettableListenableFuture;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * REST Interface for LT service execution.
 *
 * @author galanisd
 */
@RestController
public class ProcessingRESTController {

  private final static Logger log = LoggerFactory.getLogger(ProcessingRESTController.class);

  private LTServiceExecution ltServiceExecution;

  /**
   * Initializes the REST interface.
   *
   * @param ltServiceExecution
   */
  @Autowired
  public ProcessingRESTController(LTServiceExecution ltServiceExecution) {
    this.ltServiceExecution = ltServiceExecution;
  }

  /**
   * Process a document and return the result.
   */
  @CrossOrigin
  @PostMapping(RESTServices.PROCESS_DOCUMENT_SERV)
  @ApiOperation(value = "Process a document and return the result.")
  public ResponseEntity<ResponseMessage> processDocument(@RequestBody RequestMessage reqMessage, @PathVariable(RESTServices.ltService) String ltServ) {
    ResponseMessage doc = ltServiceExecution.dispatchItForProcessingAndWaitForResponse(reqMessage, ltServ);
    log.info("return:" + doc.getResponse().toString());
    return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(doc);
  }

  /**
   * Process a text request, in the ELG JSON format.
   */
  @CrossOrigin
  @PostMapping(value = RESTServices.PROCESS_TEXT, consumes = {"application/json"})
  @ApiOperation(value = "Process a document async. and return the result.")
  public ListenableFuture<ResponseEntity<?>> processTextRequest(@RequestBody RequestMessage requestMessage, @PathVariable(RESTServices.ltService) String ltServ) {
    log.info("Got a " + RESTServices.PROCESS_TEXT);
    if(requestMessage.getRequest() instanceof TextRequest) {
      return dispatchRequest(requestMessage, ltServ);
    } else {
      SettableListenableFuture<ResponseEntity<?>> slf = new SettableListenableFuture<>();
      slf.set(ResponseEntity.badRequest().body(
              new Failure().withErrors(StandardMessages.elgRequestTypeUnsupported(
                      requestMessage.getRequest().type()))
                      .asMessage(requestMessage.getMetadata())));
      return slf;
    }
  }

  /**
   * Process a plain or HTML text upload as an ELG TextRequest.
   */
  @CrossOrigin
  @PostMapping(value = RESTServices.PROCESS_TEXT, consumes = {"text/plain", "text/html"})
  @ApiOperation(value = "Process a document async. and return the result.")
  public ListenableFuture<ResponseEntity<?>> processTextOnly(@RequestBody String data, @PathVariable(RESTServices.ltService) String ltServ, @RequestHeader("Content-Type") String contentType, @RequestParam MultiValueMap<String, String> queryParams) {
    TextRequest request = new TextRequest().withContent(data).withMimeType(contentType);
    addParams(queryParams, request);
    return dispatchRequest(request.asMessage(new Metadata().withId("uploaded")), ltServ);
  }

  private void addParams(@RequestParam MultiValueMap<String, String> queryParams, Request<?> request) {
    if(queryParams != null && !queryParams.isEmpty()) {
      // turn URL query parameters into entries in the "params" map
      Map<String, Object> params = new LinkedHashMap<>();
      queryParams.forEach((key, valueList) -> {
        if(valueList.size() == 1) {
          params.put(key, valueList.get(0));
        } else {
          params.put(key, valueList);
        }
      });
      request.setParams(params);
    }
  }

  /**
   * A stub for processing resources that either live in the repo or are directy uploaded.
   */
  @PostMapping(RESTServices.PROCESS_RESOURCE_SERV)
  public void processResource() {
    // TO-DO
    //return "TO-DO";
  }

  /**
   * Process uploaded WAV audio as an ELG AudioRequest.
   */
  @CrossOrigin
  @PostMapping(value = RESTServices.PROCESS_AUDIO, consumes = {"audio/x-wav", "audio/wav"})
  @ApiOperation(value = "Process an audio request. and return the result.")
  public ListenableFuture<ResponseEntity<?>> processWAVOnly(@RequestBody byte[] audio, @PathVariable(RESTServices.ltService) String ltServ, @RequestParam MultiValueMap<String, String> queryParams) {
    log.info("Got a " + RESTServices.PROCESS_AUDIO);
    return doAudio(audio, ltServ, AudioRequest.Format.LINEAR16, queryParams);
  }

  /**
   * Process uploaded MP3 audio as an ELG AudioRequest.
   */
  @CrossOrigin
  @PostMapping(value = RESTServices.PROCESS_AUDIO, consumes = {"audio/mpeg"})
  @ApiOperation(value = "Process an audio request. and return the result.")
  public ListenableFuture<ResponseEntity<?>> processMP3Only(@RequestBody byte[] audio, @PathVariable(RESTServices.ltService) String ltServ, @RequestParam MultiValueMap<String, String> queryParams) {
    return doAudio(audio, ltServ, AudioRequest.Format.MP3, queryParams);
  }

  private ListenableFuture<ResponseEntity<?>> doAudio(@RequestBody byte[] audio, @PathVariable(RESTServices.ltService) String ltServ, AudioRequest.Format format, @RequestParam MultiValueMap<String, String> queryParams) {
    AudioRequest request = new AudioRequest().withContent(audio).withFormat(format);
    addParams(queryParams, request);
    RequestMessage message = request.asMessage(new Metadata().withId("uploaded"));
    return dispatchRequest(message, ltServ);
  }


  private ListenableFuture<ResponseEntity<?>> dispatchRequest(RequestMessage requestMessage, String ltServ) {
    // Dispatch message.
    ListenableFuture<ResponseMessage> responseft = ltServiceExecution.dispatchItForProcessing(requestMessage, ltServ);

    // Map the response message callback into a ResponseEntity with the appropriate HTTP code (200/500)
    SettableListenableFuture<ResponseEntity<?>> slfToBeReturned = new SettableListenableFuture<>();

    Map<String, Object> requestParams = requestMessage.getRequest().getParams();
    Object audioOnlyParam = (requestParams == null ? null : requestParams.get("audioOnly"));
    boolean audioOnly = (Boolean.TRUE.equals(audioOnlyParam))
            || (BooleanUtils.toBoolean(String.valueOf(audioOnlyParam)));
    responseft.addCallback((responseMessage) -> {
      if(responseMessage.getFailure() != null) {
        // this was a failure response, so 500
        slfToBeReturned.set(ResponseEntity.status(500).body(responseMessage));
      } else {
        // successful response, so return 200
        if(responseMessage.getResponse() instanceof AudioResponse && audioOnly) {
          ResponseEntity.BodyBuilder bb = ResponseEntity.ok();
          switch(((AudioResponse) responseMessage.getResponse()).getFormat()) {
            case MP3: bb.header(HttpHeaders.CONTENT_TYPE, "audio/mpeg"); break;
            case LINEAR16: bb.header(HttpHeaders.CONTENT_TYPE, "audio/x-wav"); break;
            default: bb.header(HttpHeaders.CONTENT_TYPE, "application/octet-stream"); break;
          }
          slfToBeReturned.set(bb.body(((AudioResponse) responseMessage.getResponse()).getContent()));
        } else {
          slfToBeReturned.set(ResponseEntity.ok(responseMessage));
        }
      }
    }, (throwable) -> {
      // some other error we weren't prepared for - this shouldn't happen but if
      // it does then return something sensible
      slfToBeReturned.set(ResponseEntity.status(500).body(new Failure().withErrors(
              StandardMessages.elgServiceInternalError(throwable.getMessage()))
              .asMessage(requestMessage.getMetadata())));
    });

    log.info("returning future {}", slfToBeReturned);

    return slfToBeReturned;
  }



}
