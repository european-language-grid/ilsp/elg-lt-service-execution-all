package elg.ltserviceexecution.restserver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import elg.ltserviceexecution.api.ProcessingQueue;
import elg.ltserviceexecution.common.RESTServices;
import elg.ltserviceexecution.kubernetes.k8s;
import eu.elg.model.ResponseMessage;
import io.swagger.annotations.ApiOperation;

/**
 * Processing Management REST Controller
 * 
 * @author galanisd
 *
 */
@RestController
public class ProcessingManagementRESTController {

	private final static Logger log = LoggerFactory.getLogger(ProcessingManagementRESTController.class);

	private ProcessingQueue processingQueue;
	private k8s k8sclient;
	
	@Autowired
	public ProcessingManagementRESTController(ProcessingQueue processingQueue, k8s k8sclient){
		this.processingQueue = processingQueue;
		this.k8sclient = k8sclient;
	}
		
    @GetMapping(value=RESTServices.REGISTER_SERVICE_SERV)
	@ApiOperation(value = "Register service.")
	public ResponseEntity<String>  registerService(@PathVariable(RESTServices.ltService) String ltServ){  	
    	boolean done = processingQueue.registerService(ltServ);
    	return ResponseEntity.ok().body(done + "");
	}
    
    @GetMapping(value=RESTServices.UNREGISTER_SERVICE_SERV)
	@ApiOperation(value = "Unregister service.")
	public ResponseEntity<String>  unregisterService(@PathVariable(RESTServices.ltService) String ltServ){
    	boolean done = processingQueue.unregisterService(ltServ);
    	return ResponseEntity.ok().body(done + "");

	}
}
