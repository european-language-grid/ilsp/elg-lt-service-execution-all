package elg.ltserviceexecution.restserver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;

/**
 * The main class for starting a Spring application
 * 
 * @author galanisd
 *
 */
public class Main {

	private final static Logger log = LoggerFactory.getLogger(Main.class);
	
	public static void main(String[] args) {		
		log.info("Starting " + Main.class.getName());
						
		// Run app within a Spring Context.
		SpringApplication springApplication = new SpringApplication(AppRunner.class);		
		springApplication.run();
	}
}
