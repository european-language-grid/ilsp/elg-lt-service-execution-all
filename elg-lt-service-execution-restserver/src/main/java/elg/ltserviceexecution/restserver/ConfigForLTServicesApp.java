package elg.ltserviceexecution.restserver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.amqp.RabbitProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import elg.ltserviceexecution.api.LTServiceExecution;
import elg.ltserviceexecution.api.LTServiceExecutionImpl;
import elg.ltserviceexecution.api.ProcessingQueue;
import elg.ltserviceexecution.api.RabbitMessageProducer;
import elg.ltserviceexecution.api.RabbitProcessingQueue;
import elg.ltserviceexecution.api.ResultPre;
import elg.ltserviceexecution.kubernetes.k8s;
import elg.ltserviceexecution.progressmonitor.TasksTable;
import elg.ltserviceexecution.progressmonitor.TasksTableInMemImpl;
//import elg.ltserviceexecution.api.MessageReceiver;

/**
 * Configures the LT Service Execution REST Server.
 * 
 * @author galanisd
 *
 */
@Configuration
@EnableConfigurationProperties(RabbitProperties.class)
public class ConfigForLTServicesApp {

	private final static Logger LOGGER = LoggerFactory.getLogger(ConfigForLTServicesApp.class);
	
	@Autowired
	private RabbitProperties config;
	
	@Bean
	public LTServiceExecution ltServiceExecution() {
		return new LTServiceExecutionImpl(processingQueue(), getTasks());
	}

	@Bean ResultPre getResultPre(){
		return new ResultPre();
	}
	
	@Bean
	public ProcessingQueue processingQueue() {
		return new RabbitProcessingQueue(rabbitMessageProducer(), amqpAdmin());
	}

	@Bean
	public RabbitMessageProducer rabbitMessageProducer() {
		return new RabbitMessageProducer(rabbitTemplate(), getResultPre());
	}
    
	@Bean
	public RabbitTemplate rabbitTemplate() {
		RabbitTemplate template = new RabbitTemplate(connectionFactory());
		template.setMessageConverter(jsonMessageConverter());
		template.setReplyTimeout(60000); 
		
		return template;
	}
	
	@Bean
	public ConnectionFactory connectionFactory() {		
		CachingConnectionFactory connectionFactory = new CachingConnectionFactory(config.getHost());
		// Configure
		connectionFactory.setUsername(config.getUsername());
		connectionFactory.setPassword(config.getPassword());
		
		LOGGER.info("username|" + config.getUsername() + "|");
		LOGGER.info("password|" + config.getPassword() + "|");
		
		if(config.getVirtualHost() != null){
			connectionFactory.setVirtualHost(config.getVirtualHost());
			LOGGER.info("VirtualHost|" + config.getVirtualHost() + "|");
		}else{
			LOGGER.info("VirtualHost is null");
		}
				
		return connectionFactory;
	}

	@Bean
	public Jackson2JsonMessageConverter jsonMessageConverter() {
		return new Jackson2JsonMessageConverter();
	}
	
	// ==== ===== 
	
	@Bean
	public AmqpAdmin amqpAdmin() {
	    return new RabbitAdmin(connectionFactory());
	}
	
	@Bean
	public k8s getK8s(){
		return new k8s();
	}
	
	@Bean
	public TasksTable getTasks(){
		return new TasksTableInMemImpl();
	}
}
