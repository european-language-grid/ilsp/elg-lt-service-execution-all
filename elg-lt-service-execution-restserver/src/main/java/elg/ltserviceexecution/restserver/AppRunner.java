package elg.ltserviceexecution.restserver;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
@ComponentScan(basePackages = {"elg.ltserviceexecution.restserver", "elg.ltserviceexecution.restserver.jwt", "elg.ltserviceexecution.progressmonitor"})
public class AppRunner {

}
