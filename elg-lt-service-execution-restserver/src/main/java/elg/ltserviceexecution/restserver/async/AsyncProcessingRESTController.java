package elg.ltserviceexecution.restserver.async;

import elg.ltserviceexecution.api.LTServiceExecution;
import elg.ltserviceexecution.common.RESTServices;
import elg.ltserviceexecution.progressmonitor.Task;
import elg.ltserviceexecution.progressmonitor.TasksTable;
import eu.elg.model.*;
import eu.elg.model.requests.AudioRequest;
import eu.elg.model.requests.TextRequest;
import eu.elg.model.responses.StoredResponse;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.SettableListenableFuture;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Collections;
import java.util.Optional;

@RestController
public class AsyncProcessingRESTController {

  private final static Logger log = LoggerFactory.getLogger(AsyncProcessingRESTController.class);

  private LTServiceExecution ltServiceExecution;

  private TasksTable tasksTable;

  @Value("${elg.base.url:#{null}}")
  private String baseUrl;

  /**
   * Initializes the REST interface.
   *
   * @param ltServiceExecution
   */
  @Autowired
  public AsyncProcessingRESTController(LTServiceExecution ltServiceExecution, TasksTable tasksTable) {
    this.ltServiceExecution = ltServiceExecution;
    this.tasksTable = tasksTable;
  }


  /**
   * Process a text request, in the ELG JSON format.
   */
  @CrossOrigin
  @PostMapping(value = RESTServices.ASYNC_PREFIX + RESTServices.PROCESS_TEXT, consumes = {"application/json"})
  @ApiOperation(value = "Process a document async. and return the URL that should be polled for progress.")
  public ResponseEntity<ResponseMessage> processTextRequest(@RequestBody RequestMessage requestMessage, @PathVariable(RESTServices.ltService) String ltServ) {
    log.info("Got an async " + RESTServices.PROCESS_TEXT);
    if(requestMessage.getRequest() instanceof TextRequest) {
      return dispatchRequest(requestMessage, ltServ);
    } else {
      return ResponseEntity.badRequest().body(
              new Failure().withErrors(StandardMessages.elgRequestTypeUnsupported(
                      requestMessage.getRequest().type()))
                      .asMessage(requestMessage.getMetadata()));
    }
  }

  /**
   * Process a plain or HTML text upload as an ELG TextRequest.
   */
  @CrossOrigin
  @PostMapping(value = RESTServices.ASYNC_PREFIX + RESTServices.PROCESS_TEXT, consumes = {"text/plain", "text/html"})
  @ApiOperation(value = "Process a document async. and return the URL that should be polled for progress.")
  public ResponseEntity<ResponseMessage> processTextOnly(@RequestBody String data, @PathVariable(RESTServices.ltService) String ltServ, @RequestHeader("Content-Type") String contentType) {
    TextRequest request = new TextRequest().withContent(data).withMimeType(contentType);
    return dispatchRequest(request.asMessage(new Metadata().withId("uploaded")), ltServ);
  }


  /**
   * Process uploaded WAV audio as an ELG AudioRequest.
   */
  @CrossOrigin
  @PostMapping(value = RESTServices.ASYNC_PREFIX + RESTServices.PROCESS_AUDIO, consumes = {"audio/x-wav", "audio/wav"})
  @ApiOperation(value = "Process an audio request. and return the URL that should be polled for progress.")
  public ResponseEntity<ResponseMessage> processWAVOnly(@RequestBody byte[] audio, @PathVariable(RESTServices.ltService) String ltServ) {
    log.info("Got a " + RESTServices.PROCESS_AUDIO);
    return doAudio(audio, ltServ, AudioRequest.Format.LINEAR16);
  }

  /**
   * Process uploaded MP3 audio as an ELG AudioRequest.
   */
  @CrossOrigin
  @PostMapping(value = RESTServices.ASYNC_PREFIX + RESTServices.PROCESS_AUDIO, consumes = {"audio/mpeg"})
  @ApiOperation(value = "Process an audio request. and return the URL that should be polled for progress.")
  public ResponseEntity<ResponseMessage> processMP3Only(@RequestBody byte[] audio, @PathVariable(RESTServices.ltService) String ltServ) {
    return doAudio(audio, ltServ, AudioRequest.Format.MP3);
  }

  private ResponseEntity<ResponseMessage> doAudio(byte[] audio, String ltServ, AudioRequest.Format format) {
    AudioRequest request = new AudioRequest().withContent(audio).withFormat(format);
    RequestMessage message = request.asMessage(new Metadata().withId("uploaded"));
    return dispatchRequest(message, ltServ);
  }


  private ResponseEntity<ResponseMessage> dispatchRequest(RequestMessage requestMessage, String ltServ) {
    String taskId = ltServiceExecution.dispatchItForProcessingAsync(requestMessage, ltServ);
    UriComponentsBuilder builder = (baseUrl == null ? ServletUriComponentsBuilder.fromCurrentContextPath()
            : UriComponentsBuilder.fromHttpUrl(baseUrl));
    URI pollUrl = builder.path(RESTServices.ASYNC_PREFIX + RESTServices.POLL).build(Collections.singletonMap(RESTServices.taskId, taskId));
    return ResponseEntity.accepted().body(new StoredResponse().withUri(pollUrl).asMessage(requestMessage.getMetadata()));
  }


  @CrossOrigin
  @GetMapping(value = RESTServices.ASYNC_PREFIX + RESTServices.POLL)
  @ApiOperation(value = "Polls for current status of an async request.")
  public ResponseEntity<ResponseMessage> pollStatus(@PathVariable(RESTServices.taskId) String taskId) {
    Task task = tasksTable.getTask(taskId);
    if(task == null) {
      return ResponseEntity.notFound().build();
    } else if(task.getLatestMessage() == null) {
      return ResponseEntity.ok(new Progress().withPercent(0.0).asMessage(task.getRequestMetadata()));
    } else {
      ResponseMessage message = task.getLatestMessage();
      if(message.getProgress() != null) {
        // progress message
        return ResponseEntity.ok(message);
      } else {
        tasksTable.deleteTask(taskId);
        if(message.getFailure() != null) {
          return ResponseEntity.status(500).body(message);
        } else {
          return ResponseEntity.ok(message);
        }
      }
    }
  }
}
