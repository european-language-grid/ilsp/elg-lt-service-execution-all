package elg.ltserviceexecution.restserver.jwt;

public class JWTConstants {
	    public static final long EXPIRATION_TIME = 864_000_000; // 10 days
	    public static final String TOKEN_PREFIX = "Bearer ";
	    
	    //https://stackoverflow.com/questions/22229996/basic-http-and-bearer-token-authentication?fbclid=IwAR0DKtHwgD5goysD8OV6sTYg9PqsTMFAt2XDzWJu-VUm7Lz66aM09xBR7g8
	    public static final String HEADER_STRING = "Authorization";
	   // public static final String HEADER_STRING = "A-Authorization";
}
