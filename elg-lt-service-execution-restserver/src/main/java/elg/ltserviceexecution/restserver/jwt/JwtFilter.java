package elg.ltserviceexecution.restserver.jwt;

import static elg.ltserviceexecution.restserver.jwt.JWTConstants.HEADER_STRING;
import static elg.ltserviceexecution.restserver.jwt.JWTConstants.TOKEN_PREFIX;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JwtFilter extends BasicAuthenticationFilter {

	private final static Logger LOGGER = LoggerFactory.getLogger(JwtFilter.class);

	private RestTemplate restTemplate;
	private ObjectMapper objectMapper = new ObjectMapper();
	
	//@Value("${elg.repo.location:service-elg-backend-django.elg-backend-dev}" + "/catalogue/api/accounts/verify_token/")
	private String repoEndpoint = "dev.european-language-grid.eu/catalogue/api/accounts/verify_token/"; 
	
	public JwtFilter(AuthenticationManager authManager) {
		super(authManager);
		restTemplate = new RestTemplate();
		objectMapper = new ObjectMapper();
	}

	@Override
	protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		String header = req.getHeader(HEADER_STRING);
		
		if (header == null || !header.startsWith(TOKEN_PREFIX)) {
			LOGGER.info("HEADER NOT FOUND OR DOES NOT START WITH " + TOKEN_PREFIX);
			chain.doFilter(req, res);
			return;
		}

		UsernamePasswordAuthenticationToken authentication = getAuthentication(req);
		if(authentication == null){
			chain.doFilter(req, res);
			return;
		}
		
		SecurityContextHolder.getContext().setAuthentication(authentication);
		chain.doFilter(req, res);
		
	}
	
	private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
		String token = request.getHeader(HEADER_STRING);
		token = token.replace(TOKEN_PREFIX, "").trim();

		if (token != null) {
			String user = jwtCall(token);
			if(user != null && !user.isEmpty()){
				LOGGER.info("PASSED");
				return new UsernamePasswordAuthenticationToken(user, null, new ArrayList<>());
			}else{
				LOGGER.info("TOKEN IS NULL or EMPTY");
				return null;
			}
		}
		
		return null;
	}

	private String jwtCall(String token) {
		String user = null;
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		RepoJWTRequest request = new RepoJWTRequest();
		request.setToken(token);
		
		String jsonRequest = "";
		try {
			jsonRequest = objectMapper.writeValueAsString(request);
		
			// Send & Get response
			String serviceEndpoint = "https://" + repoEndpoint;
			LOGGER.info("serviceEndpoint:" + serviceEndpoint + " jsonRequest" + jsonRequest);
			HttpEntity<String> httpRequest = new HttpEntity<String>(jsonRequest, headers);
			String response = restTemplate.postForObject(serviceEndpoint, httpRequest, String.class);
			LOGGER.info("response:" + response);
			
			// Parse response
			JsonNode root = null;
			root = objectMapper.readTree(response);
			String verified = root.path("verified").asText();
			if(verified.equalsIgnoreCase("true")){
				user = root.path("username").asText();
			}
		}catch (JsonProcessingException e) {
			LOGGER.info(e.getMessage());
		}catch (IOException e) {
			LOGGER.info(e.getMessage());
		}catch (Exception e) {
			LOGGER.info(e.getMessage());
		}
		
		return user;
	}

}