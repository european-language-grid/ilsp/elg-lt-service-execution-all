package elg.ltserviceexecution.restserver.jwt;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RepoJWTRequest {

	private String token;

	@JsonProperty("token")
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
