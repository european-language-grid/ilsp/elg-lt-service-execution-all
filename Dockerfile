# Base image.
FROM openjdk:8u111-jdk-alpine
#VOLUME /tmp

# SET TARGET DIR
ENV TARGETDIR /elg/

# Create target dir.
RUN mkdir $TARGETDIR
# Copy everything to target dir.
COPY . $TARGETDIR
# Copy/Rename server app jar.
ADD /elg-lt-service-execution-restserver/target/elg-lt-service-execution-restserver-0.0.1-SNAPSHOT-exec.jar ${TARGETDIR}dockerCmd/app.jar

# Set working dir. 
WORKDIR ${TARGETDIR}dockerCmd
#/elg/

# Start
ENTRYPOINT ["sh", "runInContainer.sh"]
