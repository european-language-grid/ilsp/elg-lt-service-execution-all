package elg.ltserviceexecution.messages;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

/**
 * @author galanisd
 *
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class DocumentProcessingReq {
	
	private long id;
    private String content;
    //private String mimeType;
        
    public DocumentProcessingReq(){
    	
    }
    
	public DocumentProcessingReq(long id, String content) {
		super();
		this.id = id;
		this.content = content;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "DocumentProcessingReq [id=" + id + ", content=" + content + "]";
	}
        
    
}
