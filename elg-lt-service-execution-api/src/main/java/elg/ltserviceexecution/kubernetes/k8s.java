package elg.ltserviceexecution.kubernetes;


import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.kubernetes.client.ApiClient;
import io.kubernetes.client.ApiException;
import io.kubernetes.client.Configuration;
import io.kubernetes.client.apis.CoreV1Api;
import io.kubernetes.client.models.V1Pod;
import io.kubernetes.client.models.V1PodList;
import io.kubernetes.client.util.ClientBuilder;

public class k8s {

	private final static Logger LOGGER = LoggerFactory.getLogger(k8s.class);
	
    private ApiClient client;
    private CoreV1Api api;

	/**
	 * Constructor
	 */
	public k8s(){
		try {
			client = ClientBuilder.defaultClient();			
			Configuration.setDefaultApiClient(client);
		    api = new CoreV1Api();
		    
		} catch (IOException e) {
			LOGGER.info("error" + e.getMessage());
		}
	}
	
	
	public void foo(){
		try{
			V1PodList list = api.listPodForAllNamespaces(null, null, null, null, null, null, null, null, null);
			for (V1Pod item : list.getItems()) {
				LOGGER.info(item.getMetadata().getName());
			}
		}catch(ApiException e){
			e.printStackTrace();
			LOGGER.info("-->" + e.getMessage());
			LOGGER.info("-->" + e.getCode());
			LOGGER.info("-->" + e.getResponseBody());
		}
	}
}
