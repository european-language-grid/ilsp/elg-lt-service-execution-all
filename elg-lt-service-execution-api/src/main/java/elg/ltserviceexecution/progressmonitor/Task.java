package elg.ltserviceexecution.progressmonitor;

import eu.elg.model.Metadata;
import org.springframework.util.concurrent.SettableListenableFuture;

import eu.elg.model.ResponseMessage;

public class Task {

	private SettableListenableFuture<ResponseMessage> slfRM;

	private ResponseMessage latestMessage;

	private Metadata requestMetadata;

	public SettableListenableFuture<ResponseMessage> getSlfRM() {
		return slfRM;
	}

	public void setSlfRM(SettableListenableFuture<ResponseMessage> slfRM) {
		this.slfRM = slfRM;
	}

	public ResponseMessage getLatestMessage() {
		return latestMessage;
	}

	public void setLatestMessage(ResponseMessage latestMessage) {
		this.latestMessage = latestMessage;
	}

	public Metadata getRequestMetadata() {
		return requestMetadata;
	}

	public void setRequestMetadata(Metadata requestMetadata) {
		this.requestMetadata = requestMetadata;
	}
}
