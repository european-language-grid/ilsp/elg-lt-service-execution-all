package elg.ltserviceexecution.progressmonitor;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.LoadingCache;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

public class TasksTableInMemImpl implements TasksTable{


	private ConcurrentMap<String, Task> tasks;

	public TasksTableInMemImpl() {
		this.tasks = CacheBuilder.newBuilder().expireAfterAccess(10, TimeUnit.MINUTES).<String, Task>build().asMap();
	}
	
	public String addTask(Task task){
		String taskID = UUID.randomUUID().toString();
		tasks.put(taskID, task);
		return taskID;
	}
	
	public Task deleteTask(String Id){
		return tasks.remove(Id);
	}

	public Task getTask(String id) {
		return tasks.get(id);
	}
}
