package elg.ltserviceexecution.progressmonitor;

/**
 * TasksTable
 * @author ilsp
 *
 */
public interface TasksTable {

	public String addTask(Task t);
	
	public Task deleteTask(String Id);

	public Task getTask(String id);
}
