package elg.ltserviceexecution.progressmonitor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.SettableListenableFuture;
import org.springframework.amqp.core.Message;

import elg.ltserviceexecution.api.RabbitProcessingQueue;
import elg.ltserviceexecution.common.QueuesConstants;
import eu.elg.model.ResponseMessage;

@Component
public class ProgressMonitor {

	private final static Logger log = LoggerFactory.getLogger(ProgressMonitor.class);

	@Autowired
	private TasksTable taskTable;

	public ProgressMonitor() {

	}
	
	/**
	   * Rabbit message listener that receives progress, success and failure reply messages
	   * from workers and handles them appropriately.
	   *
	   * @param responseMessage the parsed ResponseMessage from the JSON message body
	   * @param amqpMessage the raw AMQP message, so we can access the properties like correlation ID
	   */
	  @RabbitListener(queues = "${elg.reply.queue:"+ QueuesConstants.DEFAULT_REPLY_QUEUE +"}")
	  public void receiveMessage(ResponseMessage responseMessage, Message amqpMessage) {
	    // extract the correlation ID for this exchange
	    String correlationID = amqpMessage.getMessageProperties().getCorrelationId();
			Task t = taskTable.getTask(correlationID);
			if(t == null) {
				log.warn("Received message for unknown correlation ID {}", correlationID);
				return;
			}
	    if(responseMessage.getProgress() != null) {
	      // this is a partial progress message, so don't complete the Future at this stage
	      log.info("Progress message for correlation {}: {}", correlationID, responseMessage.getProgress().getPercent());
	     	t.setLatestMessage(responseMessage);
	    } else {
	      // this is a completion message (either failure or success), so complete the Future
	      SettableListenableFuture<ResponseMessage> responseFuture = t.getSlfRM();
	      if(responseFuture != null) {
	        log.info("Completing response for correlation {}", correlationID);
	        // the following line will release the HTTP response to the original caller
					taskTable.deleteTask(correlationID);
	        responseFuture.set(responseMessage);
	      } else {
	      	// async request - store the response
	      	t.setLatestMessage(responseMessage);
	      }
	    }
	  }
}
