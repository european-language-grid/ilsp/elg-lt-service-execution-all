package elg.ltserviceexecution.api;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import elg.ltserviceexecution.common.QueuesConstants;
import eu.elg.model.RequestMessage;
import eu.elg.model.ResponseMessage;

/**
 * Rabbit Processing Queue
 * 
 * @author galanisd
 *
 */
public class RabbitProcessingQueue implements ProcessingQueue{

	private final static Logger log = LoggerFactory.getLogger(RabbitProcessingQueue.class);

	private RabbitMessageProducer rabbitProducer;
	private AmqpAdmin amqpAdmin;
	
	@Value("${elg.reply.queue:"+ QueuesConstants.DEFAULT_REPLY_QUEUE +"}")
	private String replyAddress;
	
	private FanoutExchange alternateExchangeForRequestQueue;
	private DirectExchange exchangeForRequestQueue;
	
	/**
	 * RabbitProcessingQueue constructor.
	 * 
	 * @param rabbitProducer
	 * @param amqpAdmin
	 */
	@Autowired
	public RabbitProcessingQueue(RabbitMessageProducer rabbitProducer, AmqpAdmin amqpAdmin){
		this.rabbitProducer = rabbitProducer;
		this.amqpAdmin = amqpAdmin;
		//createConfigureRabbit();
	}	
	
	/**
	 * Configure Rabbit
	 */
	@PostConstruct 
	private void createConfigureRabbit(){	
		log.info("createConfigureRabbit");
		// Create alter exchange.
		//https://www.rabbitmq.com/ae.html
		// Alter. first
		alternateExchangeForRequestQueue = new FanoutExchange(QueuesConstants.ELG_EXCHANGE + "ALTER", true, false);
		// ELG exchange then
		Map<String, Object> args = new HashMap<String, Object>();
		//args.put("alternate-exchange", QueuesConstants.ELG_EXCHANGE_ALTER);
		exchangeForRequestQueue = new DirectExchange(QueuesConstants.ELG_EXCHANGE, true, false, args);
		// Declare them.
		log.info("declareExchange:" + alternateExchangeForRequestQueue.getName());		
		amqpAdmin.declareExchange(alternateExchangeForRequestQueue);				
		log.info("declareExchange:" + exchangeForRequestQueue.getName());
		//boolean deleteExch = amqpAdmin.deleteExchange(exchangeForRequestQueue.getName());
		//log.info("deleteExch:" + deleteExch);
		amqpAdmin.declareExchange(exchangeForRequestQueue);		
		
		// Add reply queue.
		Queue replyQueue = new Queue(replyAddress, true);			
		amqpAdmin.declareQueue(replyQueue);
		log.info("declareQueue:" + replyQueue.getActualName());

	}
	
	@Override
	public boolean registerService(String ltServ) {
		log.info("registerService:" + ltServ);		
		Queue requestQueue = new Queue(Utils.getInEndpoint(ltServ), true);
		
		//if(amqpAdmin.getQueueProperties(requestQueue.getActualName()) == null){
			log.info("declareQueue:" + requestQueue.getActualName());
			amqpAdmin.declareQueue(requestQueue);
			log.info("declareBinding:" + exchangeForRequestQueue.getName());
			amqpAdmin.declareBinding(BindingBuilder.bind(requestQueue).to(exchangeForRequestQueue).with(Utils.getInEndpoint(ltServ)));		
			log.info("registerService:" + ltServ + " OK");	
			return true;
		//}else{
		//	log.info("registerService:" + ltServ + " exists");	
		//	return false;
		//}
	}

	@Override
	public boolean unregisterService(String ltServ) {
		log.info("unregisterService:" + ltServ);		
		Queue requestQueue = new Queue(Utils.getInEndpoint(ltServ), true);	
		
		//if(amqpAdmin.getQueueProperties(requestQueue.getActualName()) != null){
			boolean deleted = amqpAdmin.deleteQueue(requestQueue.getActualName());		
			log.info("deleteQueue:" + requestQueue.getActualName() + deleted);
			log.info("removeBinding:" + exchangeForRequestQueue.getName());
			amqpAdmin.removeBinding(BindingBuilder.bind(requestQueue).to(exchangeForRequestQueue).with(Utils.getInEndpoint(ltServ)));
			log.info("unregisterService:" + ltServ + " deleted OK");		
			return true;
		//}else{
		//	log.info("registerService:" + ltServ + " does exists");
		//	return true;
		//}
		
	}
	
	@Override
	public ResponseMessage add(RequestMessage reqMessage, String ltServ) {		
		ResponseMessage resp = null;
		
		try{
			resp = rabbitProducer.sendMessage(reqMessage, ltServ);			
		}catch(Exception e){
			log.info("ERROR on send/receive <-> RabbitMQ",e);
		}
		return resp;
	}

	@Override
	public void addAsync(RequestMessage reqMessage, String ltServ, String taskID) {		
		rabbitProducer.sendMessageAsync(reqMessage, ltServ, replyAddress, taskID);
	}
	
}
