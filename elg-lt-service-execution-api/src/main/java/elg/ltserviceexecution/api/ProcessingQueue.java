package elg.ltserviceexecution.api;


import eu.elg.model.RequestMessage;
import eu.elg.model.ResponseMessage;

/**
 * An interface for interacting with the queue that keeps the data. 
 * @author galanisd
 *
 */
public interface ProcessingQueue {
	
	/**
	 * Register a service.
	 * 
	 * @param ltServ
	 */
	public boolean registerService(String ltServ);		
	
	/**
	 * Unregister service
	 * 
	 * @param ltServ
	 */
	public boolean unregisterService(String ltServ);
	
	/**
	 * Add a document for processing.
	 * @param documentProcessingRequest
	 * @return
	 */
	public ResponseMessage add(RequestMessage reqMessage, String ltServ);

	
	/**
	 * Add a document for processing and not wait.
	 * @param documentProcessingRequest
	 * @return
	 */
	public void addAsync(RequestMessage reqMessage, String ltServ, String taskID);
}
