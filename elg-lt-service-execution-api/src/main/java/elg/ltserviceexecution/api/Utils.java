package elg.ltserviceexecution.api;

/**
 * @author galanisd
 *
 */
public class Utils {

	public final static String inSuffix = "-in";
	
	public static String getInEndpoint(String ltServiceID){
		return ltServiceID + inSuffix;
	}
}
