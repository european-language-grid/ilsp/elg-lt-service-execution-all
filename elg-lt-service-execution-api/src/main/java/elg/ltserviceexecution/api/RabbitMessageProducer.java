package elg.ltserviceexecution.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;

import elg.ltserviceexecution.common.QueuesConstants;
import eu.elg.model.RequestMessage;
import eu.elg.model.ResponseMessage;

/**
 * RabbitMQ Message Producer.
 * 
 * @author galanisd
 *
 */
public class RabbitMessageProducer {

	private final static Logger log = LoggerFactory.getLogger(RabbitMessageProducer.class);

	private final RabbitTemplate rabbitTemplate;
	
	private ResultPre resultPre;
	
	@Autowired	
	public RabbitMessageProducer(RabbitTemplate rabbitTemplate, ResultPre resultPre){        
	    this.rabbitTemplate = rabbitTemplate;
	    this.resultPre = resultPre;
	}

	/**
	 * Send a processing request message.
	 * 
	 * @param requestMessage
	 * @param ltServ
	 * @return
	 */
	public ResponseMessage sendMessage(RequestMessage requestMessage, String ltServ) {		
		String exchangeName = QueuesConstants.ELG_EXCHANGE;
		String routingkey = Utils.getInEndpoint(ltServ);		
		log.info("Sending exchangeName:" + exchangeName + ":routingkey:" + routingkey);			
		
		ResponseMessage response = rabbitTemplate.convertSendAndReceiveAsType(exchangeName, routingkey, requestMessage, new ParameterizedTypeReference<ResponseMessage>() {});
		
		return response;
	}
	
	/**
	 * Send an async. processing request message.
	 * 
	 * @param requestMessage
	 * @param ltServ
	 * @return
	 */
	public void sendMessageAsync(RequestMessage requestMessage, String ltServ, String replyAddress, String taskID) {		
		String exchangeName = QueuesConstants.ELG_EXCHANGE;
		String routingkey = Utils.getInEndpoint(ltServ);		
		log.info("sendMessageAsync exchangeName:" + exchangeName + ":routingkey:" + routingkey);		
		
		rabbitTemplate.convertAndSend(exchangeName, routingkey, requestMessage, (message) -> {
		      message.getMessageProperties().setCorrelationId(taskID);
		      message.getMessageProperties().setReplyTo(replyAddress);
		      return message;
		    });
	}
}
