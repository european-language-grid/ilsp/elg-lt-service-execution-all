package elg.ltserviceexecution.api;

import org.springframework.util.concurrent.ListenableFuture;

import elg.ltserviceexecution.messages.DocumentProcessingReq;
import elg.ltserviceexecution.messages.DocumentProcessingRes;
import eu.elg.model.RequestMessage;
import eu.elg.model.ResponseMessage;

/**
 * 
 * @author galanisd
 *
 */
public interface LTServiceExecution {

	/**
	 * Dispatches a {@code DocumentProcessingReq} for processing and wait for results.
	 * @param documentProcessingRequest
	 */
	public ResponseMessage dispatchItForProcessingAndWaitForResponse(RequestMessage reqMessage, String ltServ);
	
	/**
	 * Dispatches a {@code DocumentProcessingReq} for processing.
	 * 
	 * @param reqMessage
	 * @param ltServ
	 * @return
	 */
	public ListenableFuture<ResponseMessage> dispatchItForProcessing(RequestMessage reqMessage, String ltServ);

	/**
	 * Dispatches a request for <i>asynchronous</i> processing (i.e. user will poll for response).
	 */
	public String dispatchItForProcessingAsync(RequestMessage reqMessage, String ltServ);

}
