package elg.ltserviceexecution.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.concurrent.SettableListenableFuture;

import elg.ltserviceexecution.messages.DocumentProcessingReq;
import elg.ltserviceexecution.messages.DocumentProcessingRes;
import elg.ltserviceexecution.progressmonitor.Task;
import elg.ltserviceexecution.progressmonitor.TasksTable;
import eu.elg.model.RequestMessage;
import eu.elg.model.ResponseMessage;

/**
 * An implementation of {@link elg.ltserviceexecution.api.LTServiceExecutionImpl} 
 * 
 * @author galanisd
 *
 */
public class LTServiceExecutionImpl implements LTServiceExecution{

	private ProcessingQueue processingQueue;
	private TasksTable tasks;
	
	@Autowired
	public LTServiceExecutionImpl(ProcessingQueue processingQueue, TasksTable tasks){
		this.processingQueue = processingQueue;
		this.tasks = tasks;
	}
	
	@Override
	public ResponseMessage dispatchItForProcessingAndWaitForResponse(RequestMessage reqMessage, String ltServ) {
		ResponseMessage result = processingQueue.add(reqMessage, ltServ);
		return result;
	}

	@Override
	public SettableListenableFuture<ResponseMessage> dispatchItForProcessing(RequestMessage reqMessage, String ltServ) {
	    // Create task.
		Task task = new Task();
		SettableListenableFuture<ResponseMessage> slf = new SettableListenableFuture<>();
	    task.setSlfRM(slf);
		String taskId = tasks.addTask(task);
		
		processingQueue.addAsync(reqMessage, ltServ, taskId);
		return slf;
	}

	public String dispatchItForProcessingAsync(RequestMessage reqMessage, String ltServ) {
		Task task = new Task();
		task.setRequestMetadata(reqMessage.getMetadata());
		String taskId = tasks.addTask(task);

		processingQueue.addAsync(reqMessage, ltServ, taskId);
		return taskId;
	}
}
