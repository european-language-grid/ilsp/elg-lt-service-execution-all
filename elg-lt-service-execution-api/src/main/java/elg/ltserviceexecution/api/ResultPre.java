package elg.ltserviceexecution.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.elg.model.ResponseMessage;

public class ResultPre {

	private final static Logger log = LoggerFactory.getLogger(RabbitProcessingQueue.class);

	private ObjectMapper objectMapper;

	public ResultPre(){
		objectMapper = new ObjectMapper();
	}
	
	public String getResult(Object resp) throws Exception{
		String result = "";
				
		if(resp instanceof ResponseMessage){
			log.info("ResponseMessage instance");
			String res = objectMapper.writeValueAsString(resp);
			result = res;
		}else{
			result = resp.toString();				
		}
		
		return result;
	}
	
}
